#!/usr/bin/python

""" A location sharing module for Falcon web framework
"""

from datetime import datetime
import json
import decimal
import logging

import falcon
from wsgiref import simple_server
from wsgiref.simple_server import make_server, WSGIServer
from SocketServer import ThreadingMixIn

import pymongo
from bson.objectid import ObjectId
import redis
import config

mongo = pymongo.MongoClient(config.MONGO_SERVER)
db = mongo[config.MONGO_DB]
cache = redis.StrictRedis(host=config.REDIS_SERVER,
                          port=config.REDIS_PORT, db=0)

class CustomJSONEncoder(json.JSONEncoder):
    """ Custom JSON Encoder
    """
    def default(self, obj):
        """
        """
        if isinstance(obj, decimal.Decimal):
            return str(obj)
        elif isinstance(obj, datetime):
            return str(obj.isoformat())
        elif isinstance(obj, ObjectId):
            return str(obj)
        else:
            return json.JSONEncoder.default(self, obj)

def make_response(resp,
                  msg,
                  status=falcon.HTTP_200,
                  content_type='application/json'):
    """ Configure a response object
    """
    resp.body = msg
    resp.content_type = content_type
    resp.status = status
    resp.set_header('Access-Control-Allow-Origin', '*')

def make_redirect(resp, status, location):
    """ Configure a redirect
    """
    resp.status = status
    resp.location = location

def make_stream(resp, stream, status=falcon.HTTP_200):
    """ Configure a stream
    """
    resp.stream = stream
    resp.status = status
    resp.set_header('Access-Control-Allow-Origin', '*')

def get_location_latest(req, resp, id_list):
    """ Return the latest location from the db
    """
    output = ''
    for i in id_list:
        key = 'location.{0}'.format(i)
        data = cache.get(key)
        if data is not None:
            return make_response(resp, data)

        criteria = {'userid': i}
        result = db.locations.find(criteria,\
                                  {'_id': False}).\
                                  sort('date', pymongo.DESCENDING).\
                                  limit(1)
        if result is not None:
            output = output + json.dumps(result[0],
                                         cls=CustomJSONEncoder) + '\n'

    return make_response(resp, output)

def get_location_stream(req, resp, id_list):
    """ Return a stream of data with locations
    """
    def generate():
        """ A function to yield a location data stream
        """
        # subscribe to channel messages
        pubsub = cache.pubsub()
        for userid in id_list:
            key = 'location.{0}'.format(userid)
            pubsub.subscribe(key)

        # start listening for new messages
        for i in pubsub.listen():
            # only handle real messages
            if i['type'] == 'message':
                # yield a stream
                data = i['data']
                yield ''.join(data) + '\n'

    # return the stream
    make_stream(resp, generate())

class LocationService(object):
    """ An object that can be handles posting and getting locations
        lattitude and longitude and headings
    """
    def on_get(self, req, resp):
        """ Get a single user or a list of users locations
            Either by returning just the latest or a live stream of locations

        """
        userid = req.get_param('userid')
        data = req.get_param('id_list')
        if userid is None and data is None:
            make_response(resp, 'No id specified', falcon.HTTP_404)

        if userid is not None:
            id_list = [userid]
        else:
            id_list = [i for i in data.split(',')]

        if req.get_param('latest') is not None:
            get_location_latest(req, resp, id_list)
        else:
            get_location_stream(req, resp, id_list)

    def on_post(self, req, resp):
        """ Post a new location for a user to the server.
            Adds the location redis and to mongo and then publishes it
            to Redis pub/sub
        """
        userid = req.get_param('userid')
        if userid is None:
            make_response(resp, 'No id specified', falcon.HTTP_404)

        sessionid = req.get_param('sessionid')
        data = json.load(req.stream)
        value = {
                'lat':data['lat'], 'lon':data['lon'],
                'head':data['head'], 'spd':data['spd'], 'alt':data['alt'],
                'userid':userid, 'date':datetime.utcnow(),
                'sessionid':sessionid
                }
        key = 'location.{0}'.format(userid)
        output = json.dumps(value, cls=CustomJSONEncoder)

        cache.set(key, output)
        cache.publish(key, output)
        postid = db.locations.insert(value)

        make_response(resp, 'OK')

class SessionService(object):
    """
    """
    def on_get(self, req, resp):
        """ Get the list of sessions for a user
        """
        userid = req.get_param('userid')
        if userid is None:
            make_response(resp, 'No id specified', falcon.HTTP_404)

        sessions = list(db.sessions.find({'userid':userid}))
        make_response(resp, json.dumps(sessions,
                            cls=CustomJSONEncoder))

    def on_post(self, req, resp):
        """ Create a new session or end an existing session for a user
        """
        userid = req.get_param('userid')
        if userid is None:
            make_response(resp, 'No id specified', falcon.HTTP_404)

        start = req.get_param('start')
        end = req.get_param('end')
        if start is None and end is None:
            make_response(resp, 'No start/end specified', falcon.HTTP_404)
        
        if start is not None:
            title = req.get_param('title')
            value = { 'userid': userid, 'startdate' : datetime.utcnow(),
                      'title': title,   'enddate' : None }
            sessionid = db.sessions.insert(value)
            output = {'sessionid':str(sessionid)} 
            make_response(resp, json.dumps(output)) 
        else:
            sessionid = req.get_param('sessionid')
            item = db.sessions.find({ 'userid' : userid, 
                                     '_id' : ObjectId(sessionid) })
            if item is None or item.count() != 1:
                make_response(resp, 'Session not found', falcon.HTTP_404)
            
            result = db.sessions.update({'_id':ObjectId(sessionid)}, 
                               {'$set': {'enddate':datetime.utcnow()}})

            output = {'sessionid':str(sessionid)} 
            make_response(resp, json.dumps(output)) 

class Status(object):
    """ Returns a status ok for load balancers
    """
    def on_get(self, req, resp):
        make_response(resp, 'OK')

class ThreadingWSGIServer(ThreadingMixIn, WSGIServer):
    """ For testing purposes so that the debug server can doesnt hang.
    """
    pass

app = api = falcon.API()
api.add_route('/api/v1/session', SessionService())
api.add_route('/api/v1/location', LocationService())
api.add_route('/api/v1/status', Status())

if __name__ == '__main__':
    httpd = simple_server.make_server('0.0.0.0', 8080, app, ThreadingWSGIServer)
    httpd.serve_forever()
